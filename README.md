```
Elasticsearch best-practices recommend to separate nodes in three roles:

Master nodes - intended for clustering management only, no data, no HTTP API
Client nodes - intended for client usage, no data, with HTTP API
Data nodes - intended for storing and indexing your data, no HTTP API
```
# configuration
Change the yaml files according your needs. Change the cluster name, kubernetes master, storage path etc. accordingly before deploying.

# Elasticsearch deployment
Deploy the instances in the following order to make sure they can properly connect:
```
kubectl create -f es-discovery-svc.yaml
kubectl create -f es-svc.yaml
kubectl create -f es-master.yaml
```
If the master is provisioned succesfully (check with kubectl logs -f pods/es-master-pod, then provision the client:
```
kubectl create -f es-client.yaml
```
If the master is provisioned successfully (check with kubectl logs -f pods/es-client-pod, then provision the data instance:
```
kubectl create -f es-data.yaml
```
If the master is provisioned succesfully (check with kubectl logs -f pods/es-data-pod, then provision the ingress configuration:
```
kubectl create -f es-ingress.yaml
```
The Elasticsearch cluster should now be availble through the ELB instance which routes to the ingress controller. A route53 CNAME is created to point elasticsearch.mydomain.com to the ELB address of the traefik controller. (http://elasticsearch.mydomain.com). If everything works properly you should see this page:
```
{
  "name" : "Stevie Hunter",
  "cluster_name" : "esdb",
  "version" : {
    "number" : "2.3.4",
    "build_hash" : "e455fd0c13dceca8dbbdbb1665d068ae55dabe3f",
    "build_timestamp" : "2016-06-30T11:24:31Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.0"
  },
  "tagline" : "You Know, for Search"
}
```

##### Scale the deployment
```
kubectl scale deployment es-master --replicas 3
kubectl scale deployment es-client --replicas 2
kubectl scale deployment es-data --replicas 2
```


original source: [kubernetes-elasticsearch-cluster](https://github.com/pires/kubernetes-elasticsearch-cluster)
